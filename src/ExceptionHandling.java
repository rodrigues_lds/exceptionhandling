import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * WEEK 03 - EXCEPTION HANDLING AND DATA VALIDATION
 *
 * @author Eduardo Rodrigues
 * @version 1.1
 * @since 2020-09-24
 * <p>
 * Create a program that gathers input of two numbers from the keyboard in its main method and then calls the method below to calculate.
 * Write a method that divides the two numbers together (num1/num2) and returns the result.
 * The method should include a throws clause for the most specific exception possible if the user enters zero for num2.
 * In your main method, use exception handling to catch the most specific exception possible.
 * Display a descriptive message in the catch to tell the user what the problem is.
 * Give the user a chance to retry the data entry. Display a message in the final statement.
 * Run and show the output when there is no exception and when a zero in the denominator is used.
 * Rewrite the data collection so that it uses data validation to ensure an error does not occur.
 * If the user does type a zero in the denominator, display a message and give the user a chance to retry.
 * Run and show the output when there is no problem with data entry and when a zero is used in the denominator.
 * Use this class diagram to help you understand what is required.
 */

public class ExceptionHandling {

    // Set the default messages and values
    private static final String WHAT_PROGRAM_DOES_MSG = "This program divides two numbers, validates data, and handles exceptions.";
    private static final String NUMERATOR_MSG = "Insert the numerator: ";
    private static final String DENOMINATOR_MSG = "Insert the denominator: ";
    private static final String INVALID_INPUT_MSG = "Please, only numbers are accepted!\n";
    private static final String ZERO_INPUT_MSG = "Zero is not accepted!\n";
    private static final String DIVIDED_BY_ZERO_MSG = "It is not possible to divide a number by 0!";
    private static final String THE_RESULT_MSG = "The result is: ";
    private static final String NUMBER_FORMAT = "%1.1f";
    private static final String GOODBYE_MSG = "\nGoodbye ...";

    // Scanner input
    private static Scanner sc = new Scanner(System.in);

    /**
     * This method defines the entry point for the program.
     *
     * @param args to be passed to the main() method
     */
    public static void main(String[] args) {
        // Display the header
        System.out.println(WHAT_PROGRAM_DOES_MSG);
        // Result: "This program divides two numbers, validates data, and handles exceptions.

        // Get the numbers from the user
        // I decided to create an independent function named getNumberFromUser in order to:
        // a. Establish cohesion in my program;
        // b. Satisfy the requirement that says: If the user does type a zero in the denominator, ... give the user a chance to retry.
        double num1 = getNumberFromUser(NUMERATOR_MSG, true);
        double num2 = getNumberFromUser(DENOMINATOR_MSG, false);
        // Result: Two valid numbers (Numerator & Denominator) from the user to perform a division properly. (Ex. num1 = 124 & num2 = 3)

        try {
            // Display the output
            System.out.format(THE_RESULT_MSG + NUMBER_FORMAT, doDivision(num1, num2));
            // Result: the division of two numbers (Ex. 124/3 = 41.3) using only one decimal point (NUMBER_FORMAT = %1.1f)
        } catch (ArithmeticException ex) {
            // Display the error message if the denominator is zero (Divided by Zero)
            System.out.println(ex.getMessage());
            // Result: "It is not possible to divide a number by 0!"
            // Ps. This error will not be thrown because the program was rewritten in order to satisfy the following requirement:
            // Rewrite the data collection so that it uses data validation to ensure an error does not occur. (method getNumberFromUser created).
        } finally {
            // Display Goodbye message
            System.out.println(GOODBYE_MSG);
            // Result: "Goodbye ..."
        }
    }

    /**
     * This function gets a number from the user and validate the number input (DATA VALIDATION + EXCEPTION HANDLING)
     *
     * @param msgEntry   the message to be displayed to instruct the user.
     * @param acceptZero true if 0 is accepted as an input.
     * @return the inserted valid number.
     */
    private static float getNumberFromUser(String msgEntry, boolean acceptZero) {

        float numberInput = 0;
        boolean validInput = false;

        do {
            // Output the number message
            System.out.print(msgEntry);
            // Result: "Insert the numerator: " or "Insert the denominator: "

            // Check if the input is valid
            try {
                numberInput = sc.nextFloat();
                // Result: a number from the user.

                // Check if zero is accepted from the input.
                // It checks if zero is accepted and if the user input is 0.
                if ((!acceptZero) && (numberInput == 0))
                    System.out.print(ZERO_INPUT_MSG);
                    // Result: "Zero is not accepted!";
                    // Ps, this satisfies the requirement: If the user does type a zero in the denominator,
                    //     display a message and give the user a chance to retry (do while).

                // If no exception occurs, the input is valid.
                validInput = true;
                // Result: DATA VALIDATED

            } catch (InputMismatchException ex) {
                // It ignores the last input.
                sc.next();
                // Input is not valid.
                validInput = false;
                // In case the user enters letters/symbols instead of numbers a message is displayed.
                System.out.print(INVALID_INPUT_MSG);
                // Result: "Please, only numbers are accepted!";
            }

        } while (((numberInput == 0) && (!acceptZero)) || !validInput);
        // It satisfy the requirement: If the user does type a zero in the denominator ... give the user a chance to retry.

        return numberInput;
    }

    /**
     * This function divides two numbers and return its result (EXCEPTION HANDLING).
     *
     * @param num1 Numerator
     * @param num2 Denominator
     * @return division of num1 by num2
     * @throws ArithmeticException when the dominator is zero.
     */
    private static float doDivision(double num1, double num2) throws ArithmeticException {

        // This satisfy the requirement: The method should include a throws clause for the most specific exception possible if the user enters zero for num2.
        if (num2 == 0)
            throw new ArithmeticException(DIVIDED_BY_ZERO_MSG);
        // Result: an exception will be thrown to the caller (main) if num2 is 0.
        // Ps. This error will not be thrown because the program was rewritten in order to satisfy the following requirement:
        // Rewrite the data collection so that it uses data validation to ensure an error does not occur. (method getNumberFromUser created).
        // I decided to keep it in order to see how I implemented the throw exception before implementing Data Validation on method getNumberFromUser.

        double result = num1 / num2;
        return (float) (result);
        // It returns the division of two numbers.
    }
}